<?php
namespace Packt\HelloWorld\Plugin\Catalog;
use Magento\Catalog\Model\Product;

class ProductAround
{
    public function beforeSetName(\Magento\Catalog\Model\Product $subject, $name)
    {
        $name = "HelloWorld";
        return [$name];
    }

    public function afterGetName(\Magento\Catalog\Model\Product $subject, $result)
    {
        //$result = "Name of product";
        return $result;
    }
}

<?php

namespace Packt\HelloWorld\Observer;

use Magento\Framework\Event\Observer;
class ChangeName implements \Magento\Framework\Event\ObserverInterface {
    public function execute(Observer $observer) {
        $data = $observer->getData('postdata');
        $data->setText('Du lieu da duoc thay doi');
        $observer->setData('postdata', $data);
    }
}

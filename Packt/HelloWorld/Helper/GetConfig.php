<?php

namespace Packt\HelloWorld\Helper;
use Magento\Framework\View\Element\Template;

class GetConfig extends \Magento\Framework\App\Helper\AbstractHelper {
    public $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function getValueConfig() {
        return $this->scopeConfig->getValue(
            'helloworld/hellopage/source_model_test',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}

?>

<?php

namespace Packt\HelloWorld\Controller\Adminhtml\Subscription;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

class AddNew extends Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New Post'));
        return $resultPage;
    }
}

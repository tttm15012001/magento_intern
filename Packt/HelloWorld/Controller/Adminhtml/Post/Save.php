<?php

namespace Packt\HelloWorld\Controller\Adminhtml\Post;

use Packt\HelloWorld\Model\SubscriptionFactory;
use Packt\HelloWorld\Model\ResourceModel\Subscription;
use Magento\Backend\App\Action;

class Save extends Action
{
    protected $SubscriptionFactory;
    protected $CollectionResource;

    public function __construct(
        Action\Context $context,
        Subscription $collectionResource,
        SubscriptionFactory $subscriptionFactory
    ) {
        $this->SubscriptionFactory = $subscriptionFactory;
        $this->CollectionResource = $collectionResource;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $id = !empty($data['editForm']['subscription_id']) ? (int)$data['editForm']['subscription_id'] : null;

        $newData = [
            'firstname' => $data['editForm']['firstname'],
            'lastname' => $data['editForm']['lastname'],
            'email' => $data['editForm']['email'],
            'message' => $data['editForm']['message'],
            'status' => $data['editForm']['status'],
        ];

        $post = $this->SubscriptionFactory->create();

        if($id) {
            $this->CollectionResource->load($post, $id);
        }
        try {
            $post->addData($newData);
            $this->CollectionResource->save($post);
            $this->messageManager->addSuccessMessage(__('You saved the post.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('helloworld/subscription/index');
    }
}

<?php
namespace Packt\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Context;

class ShowDiscription extends \Magento\Framework\App\Action\Action {
    protected $DescriptionFactory;
    protected $CollectionFactory;
    protected $CollectionResource;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Packt\HelloWorld\Model\ResourceModel\Subscription $CollectionResource,
        //\Packt\HelloWorld\Model\ResourceModel\Subscription\CollectionFactory $collectionFactory
        \Packt\HelloWorld\Model\SubscriptionFactory $DescriptionFactory
    )
    {
        $this->DescriptionFactory = $DescriptionFactory;
        $this->CollectionResource = $CollectionResource;
        //$this->CollectionFactory = $collectionFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $status = 'pending';
        $data2 = $this->DescriptionFactory->create()->getCollection()->addFieldToFilter('status', $status);
        foreach($data2 as $value) {
            echo "<pre>";
            print_r($value->getData());
            echo "</pre>";
        }
        $data = $this->DescriptionFactory->create();
        $this->CollectionResource->load($data, 'complete', 'status');
        echo "<pre>";
        print_r($data->getData());
        echo "</pre>";
    }
}

?>

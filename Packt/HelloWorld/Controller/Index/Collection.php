<?php
namespace Packt\HelloWorld\Controller\Index;
class Collection extends \Magento\Framework\App\Action\Action {
    public function execute() {
        $productCollection = $this->_objectManager
            ->create('Magento\Catalog\Model\ResourceModel\Product\Collection')
            ->addAttributeToSelect([
                'name',
                'price',
                'SKU'
            ]);
            //->addAttributeToFilter('name', array('like' => '%L%'));
            //->setPageSize(10,1);
        //$output = $productCollection->getSelect()->__toString();
        $productCollection->setDataToAll('price', '100');
        $productCollection->save();
        echo "<table>";
            echo "<thead>";
                echo "<tr>";
                    echo "<th>"."Name"."</th>";
                    echo "<th>"."Price"."</th>";
                    //echo "<th>"."Description"."</th>";
                echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
                echo "<tr>";
                foreach ($productCollection as $product) {
                    echo "<tr>";
                        echo "<td>".$product->getName()."</td>";
                        echo "<td>".$product->getFinalPrice()."</td>";
                        //echo "<td>".$product->getData('description')."</td>";
                    echo "</tr>";
                }
            echo "</tbody>";
        echo "</table>";
        //$this->getResponse()->setBody($output);
    }
}

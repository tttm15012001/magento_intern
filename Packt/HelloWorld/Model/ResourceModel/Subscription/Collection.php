<?php
namespace Packt\HelloWorld\Model\ResourceModel\Subscription;
/**
 * Subscription Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    protected $_idFieldName = 'subscription_id';

    public function _construct() {
        $this->_init('Packt\HelloWorld\Model\Subscription',
            'Packt\HelloWorld\Model\ResourceModel\Subscription');
    }
}

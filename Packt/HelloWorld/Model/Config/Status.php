<?php

namespace Packt\HelloWorld\Model\Config;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'Pending', 'label' => __('Pending')],
            ['value' => 'Published', 'label' => __('Published')],
            ['value' => 'Complete', 'label' => __('Complete')],
            ['value' => 'Processing', 'label' => __('Processing')]
        ];
    }
}

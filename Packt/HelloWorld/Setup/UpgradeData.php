<?php
namespace Packt\HelloWorld\Setup;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface {
    /*protected $categorySetupFactory;
    public function __construct(\Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory) {
        $this->categorySetupFactory = $categorySetupFactory;
    }
    public function upgrade(ModuleDataSetupInterface $setup,
                            ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '2.0.2') < 0) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $categorySetup->addAttribute($entityTypeId,
                'helloworld_label', array(
                    'type' => 'varchar',
                    'label' => 'HelloWorld label',
                    'input' => 'text',
                    'required' => false,
                    'visible_on_front' => true,
                    'apply_to' =>
                        'simple,configurable,virtual,bundle,downloadable',
                    'unique' => false,
                    'group' => 'HelloWorld'
                )
            );
        }
    }*/
    private $customerSetupFactory;

    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {

        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '2.0.4') < 0) {
            $customerSetup->addAttribute(Customer::ENTITY, 'customer_attribute_use_script', [
                'type' => 'varchar',
                'label' => 'Use Setup Script',
                'input' => 'text',
                'source' => '',
                'required' => true,
                'visible' => true,
                'position' => 333,
                'system' => false,
                'backend' => ''
            ]);
            $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'customer_attribute_use_script')
                ->addData(['used_in_forms' => [
                    'adminhtml_customer',
                    'adminhtml_checkout',
                    'customer_account_create',
                    'customer_account_edit'
                ]
                ]);
            $attribute->save();
        }
    }
}

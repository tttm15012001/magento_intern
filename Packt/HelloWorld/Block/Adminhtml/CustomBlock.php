<?php
namespace Packt\HelloWorld\Block\Adminhtml;
use Magento\Backend\Block\Template;

class CustomBlock extends Template {
    private $postCollection;
    protected $DescriptionFactory;
    protected $CollectionResource;

    public function __construct(
        Template\Context $context,
        \Packt\HelloWorld\Model\ResourceModel\Subscription $CollectionResource,
        //\Packt\HelloWorld\Model\ResourceModel\Subscription\Collection $postCollection,
        \Packt\HelloWorld\Model\SubscriptionFactory $DescriptionFactory
    )
    {
        $this->CollectionResource = $CollectionResource;
        $this->DescriptionFactory = $DescriptionFactory;
        //$this->postCollection = $postCollection;
        parent::__construct($context);
    }

    public function getAllData() {
        $postCollection = $this->DescriptionFactory->create();
        $this->CollectionResource->load($postCollection, 'complete', 'status');
        return $postCollection;
    }

    public function greet() {
        return 'Hello Admin';
    }
}
?>

<?php
namespace Packt\HelloWorld\Block;
use Magento\Framework\View\Element\Template;
class Landingspage extends Template
{
    public function test($array) {
        $a = new \Magento\Framework\DataObject($array);
        $this->_eventManager->dispatch('change_name_before_render', ['postdata' => $a]);
        return $a;
    }

    public function getLandingsUrl()
    {
        return $this->getUrl('helloworld');
    }

    public function getIndexUrl()
    {
        return $this->getUrl('helloworld/index/index');
    }
}

require([
    'jquery',
    'Magento_Ui/js/lib/validation/validator'
], function($, validator){
    validator.addRule(
        'validate_phone_admin',
        function (value, element) {
            return /((^(\+84|84|0|0084){1})(3|5|7|8|9))+([0-9]{8})$/.test(value);
        },
        $.mage.__("Your Phone Number is invalid -- Backend")
    );
});

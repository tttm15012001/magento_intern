require(['jquery', 'Magento_Ui/js/modal/modal'], function ($, modal) {
    var options = {
        type: 'popup',
        responsive: true,
        innerScroll: true,
        title: 'Login Window',
        buttons: [{
            text: $.mage.__('Close'),
            class: 'modal-close',
            click: function (){
                this.closeModal();
            }
        }]
    };
    modal(options, $('#modal-content'));
    $('#button-login').on('click', function () {
        $("#modal-content").modal("openModal");
    })
});
require(['jquery', 'Magento_Ui/js/modal/alert', 'Magenest_Movie/requireJS/helloworld'], function ($, test, custom) {
    $('#button-click').on('click', function () {
        test({
            content: custom.hello(),
            title: 'Hello Admin a',
            //modalClass: 'alert',
            actions: {
                always: function() {
                    // do something when the modal is closed
                }
            },
            buttons: [{
                text: $.mage.__('Close'),
                class: 'action-primary action-accept',
                click: function (){
                    this.closeModal(true);
                }
            }]
        });
    })
})

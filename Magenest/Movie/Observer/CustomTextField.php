<?php
namespace Magenest\Movie\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class CustomTextField implements ObserverInterface
{
    private $request;
    private $configWriter;
    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter
    ) {
        $this->request = $request;
        $this->configWriter = $configWriter;

    }

    public function execute(EventObserver $observer)
    {
        $param = $this->request->getParams();
        $data = $param['groups']['movie_label']['fields']['text_field']['value'];
        if($data == 'Ping') $data = 'Pong';
        $this->configWriter->save('Movie/movie_label/text_field', $data);
        return $this;
    }
}

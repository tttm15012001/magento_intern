<?php
namespace Magenest\Movie\Observer;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

class RedirectToAssignedWebsiteObserver implements ObserverInterface
{
    /**
     * @var RedirectInterface
     */
    private $redirectResponse;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct
    (
        RedirectInterface $redirectResponse,
        CustomerSession $customerSession,
        StoreManagerInterface $storeManager,
        UrlInterface $url,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->redirectResponse = $redirectResponse;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;
        $this->url = $url;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute(Observer $observer)
    {
        if (!$this->customerSession->isLoggedIn()) {
            return;
        }

        if ($this->getCustomerWebsiteId() == $this->getCurrentWebsiteId()) {
            return;
        }

        /** @var \Magento\Framework\App\Action\Action $action */
        $action = $observer->getData('controller_action');
        $this->redirectResponse->redirect(
            $action->getResponse(),
            $this->getRedirectUrl()
        );
    }

    private function getRedirectUrl(): string
    {
        $currentUrl = $this->url->getUrl('*/*/*', [
            '_current' => true,
            '_use_rewrite' => true
        ]);

        $customerWebsiteId = $this->getCustomerWebsiteId();
        $redirectUrl = str_replace(
            $this->scopeConfig->getValue('web/secure/base_url', 'websites'),
            $this->scopeConfig->getValue('web/secure/base_url', 'websites', $customerWebsiteId),
            $currentUrl
        );
        if ($redirectUrl == $currentUrl) {
            $redirectUrl = str_replace(
                $this->scopeConfig->getValue('web/unsecure/base_url', 'websites'),
                $this->scopeConfig->getValue('web/unsecure/base_url', 'websites', $customerWebsiteId),
                $currentUrl
            );
        }
        $redirectUrl .= '?SID=' . $this->customerSession->getSessionId();
        return $redirectUrl;
    }

    private function getCustomerWebsiteId(): int
    {
        return $this->customerSession->getCustomer()->getWebsiteId();
    }

    private function getCurrentWebsiteId(): int
    {
        return $this->storeManager->getWebsite()->getId();
    }
}

<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class CustomFirstName implements ObserverInterface
{
    protected $customerRepository;

    public function __construct(CustomerRepositoryInterface $customerRepository) {
        $this->customerRepository = $customerRepository;
    }

    public function execute(Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        //$request = $observer->getEvent()->getRequest();
        //$postData = $request->getPostValue();

        $customer->setFirstname('Magenest');
        $phone = $customer->getCustomAttributes()['phone'];
        if(substr($phone->getValue(), 0, 3) == '+84') {
            $customer->getCustomAttributes()['phone']->setValue('0'.substr($phone->getValue(), 3, 9));
        }
        $this->customerRepository->save($customer);
        return $this;
    }
}

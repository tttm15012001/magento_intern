<?php
/**
 * Copyright © 2017 BORN . All rights reserved.
 */
namespace Magenest\Movie\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;
use \Magenest\Movie\Model\ResourceModel\DynamicRow;
use \Magenest\Movie\Model\DynamicRowFactory;

class SaveDynamicRowEx9 implements ObserverInterface
{
    const ATTR_ATTRACTION_HIGHLIGHTS_CODE = 'attraction_highlights';

    /**
     * @var  \Magento\Framework\App\RequestInterface
     */
    protected $request;
    protected $resource;
    protected $connection;
    protected $serializer;
    protected $colletionResourceDynamicRow;
    protected $colletionFactory;

    /**
     * Constructor
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResourceConnection $resource,
        SerializerInterface $serializer,
        DynamicRow $colletionResourceDynamicRow,
        DynamicRowFactory $collectionFactory
    )
    {
        $this->colletionFactory = $collectionFactory;
        $this->colletionResourceDynamicRow = $colletionResourceDynamicRow;
        $this->connection = $resource->getConnection();
        $this->request = $request;
        $this->resource = $resource;
        $this->serializer = $serializer;
    }

    public function insertMultiple($table, $data)
    {
        try {
            $tableName = $this->resource->getTableName($table);
            return $this->connection->insertMultiple($tableName, $data);
        } catch (\Exception $e) {
            //Error
        }
    }

    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getDataObject();
        $collections = $this->colletionFactory->create();
        $collection = $this->colletionResourceDynamicRow->load($collections, $product->getData('entity_id'), 'product_id');
        /*$post = $this->request->getPost();
        $post = $post['product'];*/
        $datas = $this->serializer->serialize($product->getData('Ex_9'));
        $insertData = [
            'product_id' => $product->getData('entity_id'),
            'references' => $datas
        ];
        $collections->addData($insertData);
        $collection->save($collections);
        //$this->insertMultiple('db_ex_9_customize_adminhtml', $insertData);
    }
}

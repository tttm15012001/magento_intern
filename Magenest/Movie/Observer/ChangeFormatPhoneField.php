<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class ChangeFormatPhoneField implements ObserverInterface
{
    protected $customerRepository;
    protected $customer;
    protected $customerFactory;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\ResourceModel\Customer $customer
    ) {
        $this->customer = $customer;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
    }

    public function execute(Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomerDataObject();
        $listCustomers = $this->customerFactory->create();
        $customerById = $this->customer->load($listCustomers, $customer->getId());
        if(substr($listCustomers->getData('phone'), 0, 3) == '+84') {
            $listCustomers->setData('phone', '0'.substr($listCustomers->getData('phone'), 3, 9));
        }
        $customerById->save($listCustomers);
        return $this;
    }
}

<?php

namespace Magenest\Movie\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $categorySetupFactory;
    private $customerSetupFactory;

    public function __construct(
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
    }
    public function upgrade(ModuleDataSetupInterface $setup,
                            ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '2.0.2') < 0) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $categorySetup->addAttribute($entityTypeId,
                'note', array(
                    'type' => 'varchar',
                    'label' => 'Note Ex 4',
                    'input' => 'text',
                    'required' => true,
                    'visible_on_front' => true,
                    'apply_to' =>
                        'simple,configurable,virtual,bundle,downloadable',
                    'unique' => false,
                    'group' => 'Ex 4 Customize Admin'
                )
            );
            $categorySetup->addAttribute($entityTypeId,
                'start_date', array(
                    'type' => 'datetime',
                    'label' => 'Start Date',
                    'input' => 'date',
                    'required' => false,
                    'visible_on_front' => true,
                    'apply_to' =>
                        'simple,configurable,virtual,bundle,downloadable',
                    'unique' => false,
                    'group' => 'Ex 4 Customize Admin'
                )
            );
            $categorySetup->addAttribute($entityTypeId,
                'end_date', array(
                    'type' => 'datetime',
                    'label' => 'End Date',
                    'input' => 'date',
                    'required' => false,
                    'visible_on_front' => true,
                    'apply_to' =>
                        'simple,configurable,virtual,bundle,downloadable',
                    'unique' => false,
                    'group' => 'Ex 4 Customize Admin'
                )
            );
        }
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '2.0.7') < 0) {
            $customerSetup->addAttribute('customer_address', 'region_field', [
                'label' => 'Region',
                'input' => 'select',
                'type' => 'varchar',
                'source' => 'Magenest\Movie\Model\Config\RegionSource',
                'required' => false,
                'position' => 90,
                'visible' => true,
                'system' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'frontend_input' => 'hidden',
                'backend' => ''
            ]);

            $attribute=$customerSetup->getEavConfig()
                ->getAttribute('customer_address','region_field')
                ->addData(['used_in_forms' => [
                    'adminhtml_customer_address',
                    'adminhtml_customer',
                    'adminhtml_checkout',
                    'customer_address_edit',
                    'customer_register_address',
                    'customer_address'
                ]
                ]);
            $attribute->save();
        }
    }
}

<?php

namespace Magenest\Movie\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddNewSectionCustomer implements DataPatchInterface
{
    /**
     * ModuleDataSetupInterface
     *
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * EavSetupFactory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * AddProductAttribute constructor.
     *
     * @param ModuleDataSetupInterface  $moduleDataSetup
     * @param EavSetupFactory           $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute('catalog_product',
            'note', array(
                'type' => 'varchar',
                'label' => 'Note Ex 4',
                'input' => 'text',
                'required' => true,
                'visible_on_front' => true,
                'apply_to' =>
                    'simple,configurable,virtual,bundle,downloadable',
                'unique' => false,
                'group' => 'New Section Ex 4 Customize Admin'
            )
        );
        $eavSetup->addAttribute('catalog_product',
            'start_date', array(
                'type' => 'datetime',
                'label' => 'Start Date',
                'input' => 'date',
                'required' => false,
                'timezone' => false,
                'is_used_in_grid' => true,
                //'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'visible_on_front' => true,
                'apply_to' =>
                    'simple,configurable,virtual,bundle,downloadable',
                'unique' => false,
                'group' => 'New Section Ex 4 Customize Admin'
            )
        );
        $eavSetup->addAttribute('catalog_product',
            'end_date', array(
                'type' => 'datetime',
                'label' => 'End Date',
                'input' => 'date',
                'required' => false,
                'timezone' => false,
                'is_used_in_grid' => true,
                //'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'visible_on_front' => true,
                'apply_to' =>
                    'simple,configurable,virtual,bundle,downloadable',
                'unique' => false,
                'group' => 'New Section Ex 4 Customize Admin'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}

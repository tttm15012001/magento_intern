<?php

namespace Magenest\Movie\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class NewSectionForAlcoholProduct implements DataPatchInterface
{
    /**
     * ModuleDataSetupInterface
     *
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * EavSetupFactory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * AddProductAttribute constructor.
     *
     * @param ModuleDataSetupInterface  $moduleDataSetup
     * @param EavSetupFactory           $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute('catalog_product',
            'alcoholic_strength', array(
                'type' => 'varchar',
                'label' => 'Nồng độ cồn',
                'input' => 'text',
                'required' => true,
                'visible_on_front' => true,
                'apply_to' =>
                    'simple,configurable,virtual,bundle,downloadable',
                'unique' => false,
                'group' => 'Alcohol_Product'
            )
        );
        $eavSetup->addAttribute('catalog_product',
            'volume', array(
                'type' => 'varchar',
                'label' => 'Dung tích',
                'input' => 'text',
                'required' => true,
                'visible_on_front' => true,
                'apply_to' =>
                    'simple,configurable,virtual,bundle,downloadable',
                'unique' => false,
                'group' => 'Alcohol_Product'
            )
        );
        $eavSetup->addAttribute('catalog_product',
            'origin', array(
                'type' => 'varchar',
                'label' => 'Xuất xứ',
                'input' => 'text',
                'required' => true,
                'visible_on_front' => true,
                'apply_to' =>
                    'simple,configurable,virtual,bundle,downloadable',
                'unique' => false,
                'group' => 'Alcohol_Product'
            )
        );
        $eavSetup->addAttribute('catalog_product',
            'freeze', array(
                'type' => 'varchar',
                'label' => 'Bảo quản lạnh',
                'input' => 'select',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required' => true,
                'visible_on_front' => true,
                'apply_to' =>
                    'simple,configurable,virtual,bundle,downloadable',
                'unique' => false,
                'group' => 'Alcohol_Product'
            )
        );
        $eavSetup->addAttribute('catalog_product',
            'material', array(
                'type' => 'varchar',
                'label' => 'Chất liệu',
                'input' => 'text',
                'required' => true,
                'visible_on_front' => true,
                'apply_to' =>
                    'simple,configurable,virtual,bundle,downloadable',
                'unique' => false,
                'group' => 'Alcohol_Product'
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}

<?php
namespace Magenest\Movie\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $inlineTranslation;
    protected $escaper;
    protected $transportBuilder;
    protected $logger;
    protected $dynamicRow;
    protected $dynamicRowFactory;
    protected $serializer;
    protected $_collectionEmailTemplates;
    protected $scopeConfig;

    const XML_PATH_EMAIL_RECIPIENT = 'email/header/after_purchase';

    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        \Magenest\Movie\Model\DynamicRowFactory $dynamicRowFactory,
        \Magenest\Movie\Model\ResourceModel\DynamicRow $dynamicRow,
        \Magento\Email\Model\ResourceModel\Template\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Serialize\SerializerInterface $serializer
    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $context->getLogger();
        $this->dynamicRow = $dynamicRow;
        $this->dynamicRowFactory = $dynamicRowFactory;
        $this->serializer = $serializer;
        $this->_collectionEmailTemplates = $collectionFactory;
        $this->scopeConfig = $scopeConfig;
    }

    public function sendEmail(\Magento\Sales\Model\Order $orderInformation, $items)
    {
        // get selected template name in config
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $configValue = $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope);

        // choose id template base on template name
        $collection = $this->_collectionEmailTemplates->create()->load();
        $orig_template_code = '';
        foreach ($collection as $item) {
            if($item->getData('template_id') == $configValue) {
                $orig_template_code = $item->getData('orig_template_code');
            }
        }

        $products = $this->dynamicRowFactory->create();
        $product = $this->dynamicRow->load($products, $items[0]->getData('product_id'),'product_id');
        $references = (count($products->getData()) != 0) ? $this->serializer->unserialize($products->getData('references')) : [];
        try {
            $this->inlineTranslation->suspend();
            $sender = [
                'name' => $this->escaper->escapeHtml('Ex 10'),
                'email' => $this->escaper->escapeHtml('custom_email@gmail.com'),
            ];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier($orig_template_code)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'checkOpened'  => 'Just For Test',
                    'customerName' => $orderInformation->getData('customer_firstname').' '.$orderInformation->getData('customer_lastname'),
                    'items' => (count($references) != 0) ? $references['custom_field'] : []
//                    'course_code' => $references['custom_field'][0]['code_field'],
//                    'file' => array_values($references['custom_field'][0]['files'])[0]['url'],
//                    'link' => array_values($references['custom_field'][0]['files'])[0]['url']
                ])
                ->setFrom($sender)
                ->addTo($orderInformation->getData('customer_email'))
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}

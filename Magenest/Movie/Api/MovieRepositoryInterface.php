<?php

namespace Magenest\Movie\Api;

use Magenest\Movie\Api\Data\MovieInterface;

/**
 * Interface MovieRepositoryInterface
 * @package Magenest\Movie\Api
 */
interface MovieRepositoryInterface
{
    /**
     * @param int $id
     * @return \Magenest\Movie\Api\Data\MovieInterface
     */
    public function getById($id);

    /**
     * @param \Magenest\Movie\Api\Data\MovieInterface $blog
     * @return \Magenest\Movie\Api\Data\MovieInterface
     */
    public function save(MovieInterface $blog);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id);
}

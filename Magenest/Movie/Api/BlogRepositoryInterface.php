<?php

namespace Magenest\Movie\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magenest\Movie\Api\Data\BlogInterface;

/**
 * Interface CustomManagementInterface
 * @package ViMagento\CustomApi\Api
 */
interface BlogRepositoryInterface
{
    /**
     * @param int $id
     * @return \Magenest\Movie\Api\Data\BlogInterface
     */
    public function getById($id);

    /**
     * @param \Magenest\Movie\Api\Data\BlogInterface $blog
     * @return \Magenest\Movie\Api\Data\BlogInterface
     */
    public function save(BlogInterface $blog);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magenest\Movie\Api\Data\BlogSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}

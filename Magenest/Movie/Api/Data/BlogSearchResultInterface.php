<?php

namespace Magenest\Movie\Api\Data;

/**
 * Interface BlogSearchResultInterface
 * @package Magenest\Movie\Api\Data
 */
interface BlogSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * @return \Magenest\Movie\Api\Data\BlogInterface[]
     */
    public function getItems();

    /**
     * @param \Magenest\Movie\Api\Data\BlogInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

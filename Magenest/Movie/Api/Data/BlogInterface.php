<?php

namespace Magenest\Movie\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface BlogInterface
 * @package Magenest\Movie\Api\Data
 */
interface BlogInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * @return int
     */
    public function getAuthorId();

    /**
     * @param int $authorId
     * @return $this
     */
    public function setAuthorId($authorId);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $description
     * @return $this
     */
    public function setContent($description);

    /**
     * @return string
     */
    public function getUrlRewrite();

    /**
     * @param string $urlRewrite
     * @return $this
     */
    public function setUrlRewrite($urlRewrite);

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status);
}

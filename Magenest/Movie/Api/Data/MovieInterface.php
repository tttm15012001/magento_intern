<?php

namespace Magenest\Movie\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface MovieInterface
 * @package Magenest\Movie\Api\Data
 */
interface MovieInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getMovieId();

    /**
     * @param int $movieId
     * @return $this
     */
    public function setMovieId($movieId);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return int
     */
    public function getRating();

    /**
     * @param int $rate
     * @return $this
     */
    public function setRating($rate);

    /**
     * @return int
     */
    public function getDirectorId();

    /**
     * @param int $directorId
     * @return $this
     */
    public function setDirectorId($directorId);
}

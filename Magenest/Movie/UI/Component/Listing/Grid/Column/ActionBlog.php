<?php

namespace Magenest\Movie\UI\Component\Listing\Grid\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class ActionBlog extends Column
{
    protected $urlBuilder;

    public function __construct(
        UrlInterface $urlBuilder,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = [
                    'edit' => [
                        'href' => $this->urlBuilder->getUrl('movie/post/addnewblog', ['id' => $item['id']]),
                        'label' => __('Edit')
                    ],
                    /*'delete' => [
                        'href' => $this->urlBuilder->getUrl('movie/post/deleteactor', ['id' => $item['actor_id']]),
                        'label' => __('Delete')
                    ]*/
                ];
            }
        }

        return $dataSource;
    }
}

<?php
namespace Magenest\Movie\UI\Component\Listing\Grid\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class Date extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if ($this->getData('name') == 'start_date') {
                    $item[$this->getData('name')] = date("d.m.Y H:i:s")." Uhr";
                }
            }
        }

        return $dataSource;
    }
}

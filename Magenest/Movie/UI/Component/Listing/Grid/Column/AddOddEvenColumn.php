<?php
namespace Magenest\Movie\UI\Component\Listing\Grid\Column;

use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;

class AddOddEvenColumn extends Column
{
    protected $_orderRepository;
    protected $_searchCriteria;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $criteria,
        array $components = [],
        array $data = []
    ) {
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria  = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function decorateStatus($value) {
        $class = '';
        switch ($value) {
            case 'Odd':
                $class = 'grid-severity-critical';
                break;
            case 'Even':
                $class = 'grid-severity-notice';
                break;
            case 'Undefined':
            default:
                $class = 'grid-severity-minor';
                break;
        }
        return '<span class="' . $class . '"><span>' . $value . '</span></span>';
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {

                $order  = $this->_orderRepository->get($item["entity_id"]);
                $orderId = $order->getData("entity_id");

                switch ((float)$orderId % 2) {
                    case 0:
                        $status = $this->decorateStatus('Odd');
                        break;
                    case 1;
                        $status = $this->decorateStatus('Even');
                        break;
                    default:
                        $status = $this->decorateStatus('Undefined');
                        break;
                }
                // $this->getData('name') returns the name of the column so in this case it would return export_status
                $item['odd_even'] = $status;
            }
        }
        return $dataSource;
    }
}

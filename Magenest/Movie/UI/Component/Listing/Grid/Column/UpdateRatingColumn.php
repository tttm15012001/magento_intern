<?php
namespace Magenest\Movie\UI\Component\Listing\Grid\Column;

use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class UpdateRatingColumn extends Column {
    protected $_orderRepository;
    protected $_searchCriteria;
    protected $collectionMovieFactory;

    public function __construct(
        PriceCurrencyInterface $priceCurrency,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $criteria,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionMovieFactory  $collectionMovieFactory,
        array $components = [],
        array $data = [])
    {
        $this->_orderRepository = $orderRepository;
        $this->collectionMovieFactory = $collectionMovieFactory;
        $this->_searchCriteria  = $criteria;

        $this->priceCurrency = $priceCurrency;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $movieRating = $this->collectionMovieFactory->create()->addFieldToFilter('id', $item['movie_id']);
                $ratingNum = (int)$item['rating'];
                $item['rating'] = '';
                for($i = 0; $i < 5; $i++) {
                    if($ratingNum >= 2) {
                        $item['rating'] .= '<i class="fa fa-star checked"></i>';
                    } else if($ratingNum == 1) {
                        $item['rating'] .= '<i class="fa fa-star-half-full"></i>';
                    } else {
                        $item['rating'] .= '<i class="fa fa-star"></i>';
                    }
                    $ratingNum -= 2;
                }
            }
        }
        return $dataSource;
    }
}

<?php

namespace Magenest\Movie\UI\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Controller\Adminhtml\Product\Initialization\StockDataFilter;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Textarea;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Modal;


class Another extends AbstractModifier
{
    const DISCOUNT_FREQUENCY_FIELD = 'discount_frequency'; //attribute code

    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var array
     */
    private $meta = [];

    /**
     * @var string
     */
    protected $scopeName;

    /**
     * @param LocatorInterface $locator
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        ArrayManager     $arrayManager,
                         $scopeName = ''
    )
    {
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
        $this->scopeName = $scopeName;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $fieldCode = self::DISCOUNT_FREQUENCY_FIELD;

        $model = $this->locator->getProduct();
        $modelId = $model->getId();

        $frequencyData = $model->getDiscountFrequency();

        if ($frequencyData) {
            $frequencyData = json_decode($frequencyData, true);
            $path = $modelId . '/' . self::DATA_SOURCE_DEFAULT . '/' . self::DISCOUNT_FREQUENCY_FIELD;
            $data = $this->arrayManager->set($path, $data, $frequencyData);
        }
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        $this->initDiscountFrequencyFields();
        return $this->meta;
    }


    protected function initDiscountFrequencyFields()
    {
        $frequencyPath = $this->arrayManager->findPath(
            self::DISCOUNT_FREQUENCY_FIELD,
            $this->meta,
            null,
            'children'
        );

        if ($frequencyPath) {
            $this->meta = $this->arrayManager->merge(
                $frequencyPath,
                $this->meta,
                $this->initFrquencyFieldStructure($frequencyPath)
            );
            $this->meta = $this->arrayManager->set(
                $this->arrayManager->slicePath($frequencyPath, 0, -3)
                . '/' . self::DISCOUNT_FREQUENCY_FIELD,
                $this->meta,
                $this->arrayManager->get($frequencyPath, $this->meta)
            );
            $this->meta = $this->arrayManager->remove(
                $this->arrayManager->slicePath($frequencyPath, 0, -2),
                $this->meta
            );
        }

        return $this;
    }


    protected function initFrquencyFieldStructure($frequencyPath)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'dynamicRows',
                        'label' => __('Discount Frequency'),
                        'renderDefaultRecord' => false,
                        'recordTemplate' => 'record',
                        'dataScope' => '',
                        'dndConfig' => [
                            'enabled' => false,
                        ],
                        'disabled' => false,
                        'sortOrder' =>
                            $this->arrayManager->get($frequencyPath . '/arguments/data/config/sortOrder', $this->meta),
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'dataScope' => '',
                            ],
                        ],
                    ],
                    'children' => [
                        'discount' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Input::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Discount percentage'),
                                        'dataScope' => 'discount',
                                        'require' => '1',
                                    ],
                                ],
                            ],
                        ],
                        'frequency' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Textarea::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Frequency'),
                                        'dataScope' => 'frequency',
                                        'require' => '1',
                                    ],
                                ],
                            ],
                        ],
                        'frequency_type' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Select::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Frequency Type'),
                                        'dataScope' => 'frequency_type',
                                        'options' => $this->FrequencyType(),
                                    ],
                                ],
                            ],
                        ],
                        'actionDelete' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => 'actionDelete',
                                        'dataType' => Text::NAME,
                                        'label' => '',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    public function FrequencyType()
    {
        $regionOptions = array();
        $regionOptions[] = ['label' => 'Month', 'value' => 1];
        $regionOptions[] = ['label' => 'Week', 'value' => 2];
        $regionOptions[] = ['label' => 'Year', 'value' => 3];
        return $regionOptions;
    }
}

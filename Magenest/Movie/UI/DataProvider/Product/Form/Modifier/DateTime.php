<?php
namespace Magenest\Movie\UI\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;

/**
 * Data provider for "Custom Attribute" field of product page
 */
class DateTime extends AbstractModifier
{
    /**
     * @param ArrayManager                $arrayManager
     */
    public function __construct(
        ArrayManager $arrayManager
    ) {
        $this->arrayManager = $arrayManager;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $meta = $this->enableTime($meta);

        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Customise Custom Attribute field
     *
     * @param array $meta
     *
     * @return array
     */
    protected function enableTime(array &$meta)
    {
        $fieldCode = 'start_date';

        $elementPath = $this->arrayManager->findPath($fieldCode, $meta, null, 'children');
        $containerPath = $this->arrayManager->findPath(static::CONTAINER_PREFIX . $fieldCode, $meta, null, 'children');

        if (!$elementPath) {
            return $meta;
        }

        $meta = $this->arrayManager->merge(
            $containerPath,
            $meta,
            [
                'children'  => [
                    $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'dataType' => 'date',
                                    'formElement' => 'input',
                                    'component' => 'Magenest_Movie/js/date',
                                    'input' => 'date',
                                    'options'       => [
                                        'dateFormat' => 'dd/M/yy',
                                        'timeFormat' => 'HH:mm',
                                        'showsTime' => true,
                                        'timeOnly' => false,
                                        'currentText' => 'Now'
                                    ]
                                ],
                            ],
                        ],
                    ]
                ]
            ]
        );

        return $meta;
    }
}

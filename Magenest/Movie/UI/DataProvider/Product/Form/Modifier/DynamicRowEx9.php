<?php
namespace Magenest\Movie\UI\DataProvider\Product\Form\Modifier;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\ActionDelete;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Framework\UrlInterface;
use \Magenest\Movie\Model\ResourceModel\DynamicRow;
use \Magenest\Movie\Model\DynamicRowFactory;
use Magento\Framework\Serialize\SerializerInterface;

class DynamicRowEx9 extends AbstractModifier
{
    private const MAX_FILE_SIZE = 2097152;
    const FIELD_IS_DELETE = 'is_delete';
    const FIELD_SORT_ORDER_NAME = 'sort_order';
    const FIELD_NAME_SELECT = 'select_field';

    private $locator;
    private $groupCustomer;
    private $searchCriteriaBuilder;
    protected $urlBuilder;
    protected $colletionResourceDynamicRow;
    protected $colletionFactory;
    private $serializer;

    public function __construct(
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        LocatorInterface $locator,
        UrlInterface $urlBuilder,
        DynamicRow $colletionResourceDynamicRow,
        DynamicRowFactory $collectionFactory,
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
        $this->colletionFactory = $collectionFactory;
        $this->colletionResourceDynamicRow = $colletionResourceDynamicRow;
        $this->groupCustomer = $groupRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->locator = $locator;
        $this->urlBuilder = $urlBuilder;
    }
    public function modifyData(array $data)
    {
        $dataInArr = array_values($data)[0];
        $collections = $this->colletionFactory->create();
        $collection = $this->colletionResourceDynamicRow->load($collections, $dataInArr['product']['current_product_id'], 'product_id');
        if(count($collections->getData()) != 0) {
            $data[$dataInArr['product']['current_product_id']]['product']['Ex_9'] =
                $this->serializer->unserialize($collections->getData('references'));
        }
        return $data;
    }
    public function modifyMeta(array $meta)
    {
        $meta = array_replace_recursive(
            $meta,
            [
                'Ex_9' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Ex 9'),
                                'componentType' => Fieldset::NAME,
                                'dataScope' => 'data.product.Ex_9',
                                'collapsible' => true,
                                'sortOrder' => 5,
                            ],
                        ],
                    ],
                    'children' => [
                        "custom_field" => $this->getSelectTypeGridConfig(10)
                    ],
                ]
            ]
        );
        return $meta;
    }
    protected function getSelectTypeGridConfig($sortOrder) {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add New Group'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Ui/js/dynamic-rows/dynamic-rows',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'renderDefaultRecord' => false,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'positionProvider' => static::FIELD_SORT_ORDER_NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        'code_field' => $this->getCodeFieldConfig(),
                        'files' => $this->getFilesFieldConfig(),
                        static::FIELD_IS_DELETE => $this->getIsDeleteFieldConfig(30)
                        //Add as many fields as you want

                    ]
                ]
            ]
        ];
    }
    protected function getCodeFieldConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Course Code'),
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => 'code_field',
                        'dataType' => Text::NAME,
                        'sortOrder' => 10,
                        'visible' => true,
                        'disabled' => false,
                    ]
                ],
            ],
        ];
    }
    protected function getFilesFieldConfig()
    {
        $maxFileSize = self::MAX_FILE_SIZE;
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'type' => 'text',
                        'label' => __('Files Upload'),
                        'formElement' => 'fileUploader',
                        'componentType' => 'fileUploader',
                        'maxFileSize' => $maxFileSize,
                        'previewTmpl' => 'Magenest_Movie/image-preview',
                        'elementTmpl' => 'ui/form/element/uploader/uploader',
                        'uploaderConfig' => [
                            'url' => 'movie/upload/upload'
                        ],
                        'required' => true,
                        'dataScope' => 'files',
                        'user_defined' => true,
                        'sortOrder' => 20,
                        'visible' => true,
                        'disabled' => false,
                    ]
                ],
            ],
        ];
    }
    protected function _getOptions()
    {
        $options = [
            1 => [
                'label' => __('Option 1'),
                'value' => 1
            ],
            2 => [
                'label' => __('Option 2'),
                'value' => 2
            ],
            3 => [
                'label' => __('Option 3'),
                'value' => 3
            ],
        ];

        return $options;
    }
    protected function getIsDeleteFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => ActionDelete::NAME,
                        'fit' => true,
                        'sortOrder' => $sortOrder
                    ],
                ],
            ],
        ];
    }
}

<?php

namespace Magenest\Movie\Component\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class OrderGrid extends Column
{
    public function __construct(
        ContextInterface $contextInterface,
        UiComponentFactory $componentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($contextInterface, $componentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        $dateFormat = "dd/MM/Y";

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $date = '';
                if ($item[$this->getData('name')] != '' && $item[$this->getData('name')] != '0000-00-00 00:00:00') {
                    $date = date($dateFormat, strtotime($item[$this->getData('name')]));
                }
                $item[$this->getData('name')] = $date;
            }
        }
        return $dataSource;
    }
}

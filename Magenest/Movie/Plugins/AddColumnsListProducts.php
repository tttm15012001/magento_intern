<?php
namespace Magenest\Movie\Plugins;

use Magento\Framework\Message\ManagerInterface as MessageManager;
use Magento\Framework\Registry;
use Magento\Sales\Model\ResourceModel\Order\Grid\Collection as SalesOrderGridCollection;
use Magento\Framework\ObjectManagerInterface;

class AddColumnsListProducts extends \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory
{
    private $messageManager;
    private $collection;
    private $registry;
    protected $objectManager;

    public function __construct(
        MessageManager $messageManager,
        SalesOrderGridCollection $collection,
        Registry $registry,
        ObjectManagerInterface $objectManagerInterface
    ) {
        $this->messageManager = $messageManager;
        $this->collection = $collection;
        $this->registry = $registry;
        $this->objectManager = $objectManagerInterface;
    }

    public function aroundGetReport(
        \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory $subject,
        \Closure $proceed,
        $requestName
    ) {
        $result = $proceed($requestName);
        if ($requestName == 'sales_order_grid_data_source') {
            if ($result instanceof $this->collection
            ) {
                if (is_null($this->registry->registry('list_products_joined'))) {
                    $select = $this->collection->getSelect();
                    $select->join(
                        ["soi" => "sales_order_item"],
                        'main_table.entity_id = soi.order_id',
                        ['name' => 'GROUP_CONCAT(soi.name)']
                    )->group('main_table.entity_id')->distinct();
                    $this->registry->register('list_products_joined', true);
                }
            }
        }
        else {
            $this->collection = $this->objectManager->create($subject->collections[$requestName]);
        }
        return $this->collection;
    }
}

<?php
namespace Magenest\Movie\Controller\Blog;

use Magento\Framework\UrlInterface;

class Blog1 extends \Magento\Framework\App\Action\Action
{
    protected $_urlBuilder;
    protected $urlInterface;
    protected $collectionBLogFactory;

    public function __construct(
        UrlInterface $urlBuilder,
        \Magento\Framework\App\Action\Context $context,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionBLogFactory $collectionBLogFactory,
        \Magento\Framework\UrlInterface $urlInterface
    )
    {
        $this->collectionBLogFactory = $collectionBLogFactory;
        $this->urlInterface = $urlInterface;
        $this->_urlBuilder = $urlBuilder;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->collectionBLogFactory->create();
        $columnURL = array_column($collection->getData(), 'url_rewrite');
        $columnId = array_column($collection->getData(), 'id');
        $params = explode('/', $this->_url->getUrl('*/*/*', ['_current' => true]));

        $index = array_search($params[count($params) - 2], $columnURL);

        $queryParams = [
            'id' => (int)$columnId[$index]
        ];



        //$this->_redirect($this->_urlBuilder->getUrl('movie/blog/view', ['_current' => true,'_use_rewrite' => true, '_query' => $queryParams]));
        $this->_redirect($this->_url->getUrl('movie/blog/view', ['_query' => $queryParams]));
        //$this->_redirect('Blog1');
    }
}

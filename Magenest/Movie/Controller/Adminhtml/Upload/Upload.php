<?php

namespace Magenest\Movie\Controller\Adminhtml\Upload;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magenest\Movie\Model\ImageUploader;

class Upload extends \Magento\Backend\App\Action
{
    public $imageUploader;

    public function __construct(
        Context $context,
        ImageUploader $imageUploader
    )
    {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
    }

    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Vendor_Module::label');
    }

    public function execute()
    {
        $docs = array();
        $products = isset($_FILES['product']) ? $_FILES['product'] : array();

        foreach ($products as $key => $product){
            foreach ($product as $doc){
                if($key == 'name'){
                    $docs['name'] = array_values($doc['custom_field'])[0]['files'];
                }
                if($key == 'full_path'){
                    $docs['full_path'] = array_values($doc['custom_field'])[0]['files'];
                }
                if($key == 'type'){
                    $docs['type'] = array_values($doc['custom_field'])[0]['files'];
                }
                if($key == 'tmp_name'){
                    $docs['tmp_name'] = array_values($doc['custom_field'])[0]['files'];
                }
                if($key == 'error'){
                    $docs['error'] = array_values($doc['custom_field'])[0]['files'];
                }
                if($key == 'size'){
                    $docs['size'] = array_values($doc['custom_field'])[0]['files'];
                }
            }
        }
        try {
            if(!empty($docs)) {
                $result = $this->imageUploader->saveFileToTmpDir($docs);
                $result['cookie'] = [
                    'name' => $this->_getSession()->getName(),
                    'value' => $this->_getSession()->getSessionId(),
                    'lifetime' => $this->_getSession()->getCookieLifetime(),
                    'path' => $this->_getSession()->getCookiePath(),
                    'domain' => $this->_getSession()->getCookieDomain(),
                ];
            }
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}

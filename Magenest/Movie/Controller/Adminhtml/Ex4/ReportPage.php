<?php
namespace Magenest\Movie\Controller\Adminhtml\Ex4;

class ReportPage extends \Magento\Backend\App\Action {
    private $_pageFactory;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_pageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Request Report'));
        return $resultPage;
    }
}

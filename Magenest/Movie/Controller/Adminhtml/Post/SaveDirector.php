<?php

namespace Magenest\Movie\Controller\Adminhtml\Post;

use Magenest\Movie\Model\DirectorFactory;
use Magenest\Movie\Model\ResourceModel\Director;
use Magento\Backend\App\Action;

class SaveDirector extends Action
{
    protected $SubscriptionFactory;
    protected $CollectionResource;

    public function __construct(
        Action\Context $context,
        Director $collectionResource,
        DirectorFactory $subscriptionFactory
    ) {
        $this->SubscriptionFactory = $subscriptionFactory;
        $this->CollectionResource = $collectionResource;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $id = !empty($data['director_id']) ? (int)$data['director_id'] : null;

        $newData = [
            'name' => $data['name']
        ];

        $post = $this->SubscriptionFactory->create();

        if($id) {
            $this->CollectionResource->load($post, $id);
        }
        try {
            $post->addData($newData);
            $this->CollectionResource->save($post);
            $this->messageManager->addSuccessMessage(__('You saved new director.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('movie/magenest/director');
    }
}

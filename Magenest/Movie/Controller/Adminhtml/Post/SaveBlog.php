<?php

namespace Magenest\Movie\Controller\Adminhtml\Post;

use Magenest\Movie\Model\BlogFactory;
use Magenest\Movie\Model\ResourceModel\Blog;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionBLogFactory;
use Magento\Backend\App\Action;

class SaveBlog extends Action
{
    protected $SubscriptionFactory;
    protected $CollectionResource;
    protected $collectionBLogFactory;

    public function __construct(
        Action\Context $context,
        Blog $collectionResource,
        BlogFactory $subscriptionFactory,
        CollectionBLogFactory $collectionBLogFactory
    ) {
        $this->SubscriptionFactory = $subscriptionFactory;
        $this->CollectionResource = $collectionResource;
        $this->collectionBLogFactory = $collectionBLogFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $id = !empty($data['id']) ? (int)$data['id'] : null;

        $collection = $this->collectionBLogFactory->create();
        $columnURL = array_column($collection->getData(), 'url_rewrite');

        if(in_array($data['url_rewrite'], $columnURL) && !$id) {
            $this->messageManager->addErrorMessage('Duplicate URL');
        } else {
            $newData = [
                'author_id' => $data['author_id'],
                'title' => $data['title'],
                'url_rewrite' => $data['url_rewrite'],
                'description' => $data['description'],
                'status' => $data['status'],
                'content' => $data['content'],
            ];

            $post = $this->SubscriptionFactory->create();

            if($id) {
                $this->CollectionResource->load($post, $id);
            }
            try {
                $post->addData($newData);
                $this->CollectionResource->save($post);
                $this->messageManager->addSuccessMessage(__('You saved new blog.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }

        return $this->resultRedirectFactory->create()->setPath('movie/magenest/blog');
    }


//    public function save()
//    {
//        $data = [
//            ['name' => 'blog1'],
//            ['name' => 'blog2']
//        ];
//        foreach ($data as $item) {
//            $model = $this->SubscriptionFactory->create();
//            $model->addData($item);
//            $model->save(); // model chua name laf blog1, id = 2
//        }
//
//        $contrau = new DongVat();
//        $contrau->setName('con trau ');
//        $conga = new DongVat();
//        $productCollection->getSize();
//        $productCollection->count();
//    }
}

<?php

namespace Magenest\Movie\Controller\Adminhtml\Post;

use Magenest\Movie\Model\MovieFactory;
use Magenest\Movie\Model\ResourceModel\Movie;
use Magento\Backend\App\Action;

class SaveMovie extends Action
{
    protected $SubscriptionFactory;
    protected $CollectionResource;

    public function __construct(
        Action\Context $context,
        Movie $collectionResource,
        MovieFactory $subscriptionFactory
    ) {
        $this->SubscriptionFactory = $subscriptionFactory;
        $this->CollectionResource = $collectionResource;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $id = !empty($data['movie_id']) ? (int)$data['movie_id'] : null;

        $newData = [
            'name' => $data['name'],
            'description' => $data['description'],
            'rating' => $data['rating'],
            'director_id' => $data['director_id'],
        ];

        $post = $this->SubscriptionFactory->create();

        if($id) {
            $this->CollectionResource->load($post, $id);
        }
        try {
            $post->addData($newData);
            //$this->_eventManager->dispatch('save_a_movie', ['postData' => $post]);
            $this->CollectionResource->save($post);
            $this->messageManager->addSuccessMessage(__('You saved new movie.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('movie/magenest/movie');
    }
}

<?php

namespace Magenest\Movie\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionDirectorFactory;
use Magenest\Movie\Model\ResourceModel\Director;
use Magenest\Movie\Model\DirectorFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Backend\Model\View\Result\RedirectFactory;

class DeleteDirector extends Action
{
    private $collectionResource;
    private $subscriptionFactory;
    private $filter;
    private $collectionFactory;
    private $resultRedirect;

    public function __construct(
        Action\Context $context,
        Director $collectionResource,
        DirectorFactory $subscriptionFactory,
        Filter $filter,
        CollectionDirectorFactory $collectionFactory,
        RedirectFactory $redirectFactory
    )
    {
        parent::__construct($context);
        $this->collectionResource = $collectionResource;
        $this->subscriptionFactory = $subscriptionFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->resultRedirect = $redirectFactory;
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $total = 0;
        $err = 0;
        foreach ($collection->getItems() as $item) {
            $data = $this->subscriptionFactory->create();
            $this->collectionResource->load($data, $item->getData('director_id'));
            try {
                $this->collectionResource->delete($data);
                $total++;
            } catch (LocalizedException $exception) {
                $err++;
            }
        }

        if ($total) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $total)
            );
        }

        if ($err) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted. Please see server logs for more details.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('movie/magenest/director');
    }
}

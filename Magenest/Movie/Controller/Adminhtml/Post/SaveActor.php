<?php

namespace Magenest\Movie\Controller\Adminhtml\Post;

use Magenest\Movie\Model\ActorFactory;
use Magenest\Movie\Model\ResourceModel\Actor;
use Magento\Backend\App\Action;

class SaveActor extends Action
{
    protected $SubscriptionFactory;
    protected $CollectionResource;

    public function __construct(
        Action\Context $context,
        Actor $collectionResource,
        ActorFactory $subscriptionFactory
    ) {
        $this->SubscriptionFactory = $subscriptionFactory;
        $this->CollectionResource = $collectionResource;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $id = !empty($data['actor_id']) ? (int)$data['actor_id'] : null;

        $newData = [
            'name' => $data['name'],
        ];

        $post = $this->SubscriptionFactory->create();

        if($id) {
            $this->CollectionResource->load($post, $id);
        }
        try {
            $post->addData($newData);
            $this->CollectionResource->save($post);
            $this->messageManager->addSuccessMessage(__('You saved new actor.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('movie/magenest/actor');
    }
}

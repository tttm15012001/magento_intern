<?php
namespace Magenest\Movie\Controller\Index;

use Magento\Framework\App\Action\Context;

class CollectionBlog extends \Magento\Framework\App\Action\Action {
    protected $blogFactory;
    protected $categoryFactory;
    protected $blogCategoryFactory;
    protected $blogCategory;
    protected $category;
    protected $blog;

    public function __construct(
        Context $context,
        \Magenest\Movie\Model\BlogFactory $blogFactory,
        \Magenest\Movie\Model\CategoryFactory $categoryFactory,
        \Magenest\Movie\Model\BlogCategoryFactory $blogCategoryFactory,
        \Magenest\Movie\Model\ResourceModel\BlogCategory $blogCategory,
        \Magenest\Movie\Model\ResourceModel\Category $category,
        \Magenest\Movie\Model\ResourceModel\Blog $blog
    )
    {
        $this->blog = $blog;
        $this->category = $category;
        $this->blogCategory = $blogCategory;
        $this->blogFactory = $blogFactory;
        $this->categoryFactory = $categoryFactory;
        $this->blogCategoryFactory = $blogCategoryFactory;
        parent::__construct($context);
    }

    public function execute() {
        $blog = $this->blogFactory->create();
        $category = $this->categoryFactory->create();
        $blogCategory = $this->blogCategoryFactory->create();

        // blog
        $blog->setData([
            'author_id' => 1, 'title' => 'title1', 'description' => 'description1',
            'content' => 'content1', 'url_rewrite' => 'blog1.html'
        ]);
        $this->blog->save($blog);
        $blog->setData([
            'author_id' => 2, 'title' => 'title2', 'description' => 'description2',
            'content' => 'content2', 'url_rewrite' => 'blog2.html'
        ]);
        $this->blog->save($blog);
        $blog->setData([
            'author_id' => 3, 'title' => 'title3', 'description' => 'description3',
            'content' => 'content3', 'url_rewrite' => 'blog3.html'
        ]);
        $this->blog->save($blog);
        $blog->setData([
            'author_id' => 4, 'title' => 'title4', 'description' => 'description4',
            'content' => 'content4', 'url_rewrite' => 'blog4.html'
        ]);
        $this->blog->save($blog);
        $blog->setData([
            'author_id' => 3, 'title' => 'title5', 'description' => 'description5',
            'content' => 'content5', 'url_rewrite' => 'blog5.html'
        ]);
        $this->blog->save($blog);

        // category
        $category->setData([
            'name' => 'Daily routine'
        ]);
        $this->category->save($category);
        $category->setData([
            'name' => 'One day at work'
        ]);
        $this->category->save($category);
        $category->setData([
            'name' => 'One day at school'
        ]);
        $this->category->save($category);
        $category->setData([
            'name' => 'Hobby'
        ]);
        $this->category->save($category);
        $blogCategory->setData([
            'blog_id' => 1, 'category_id' => 1
        ]);
        $this->blogCategory->save($blogCategory);
        $blogCategory->setData([
            'blog_id' => 2, 'category_id' => 2
        ]);
        $this->blogCategory->save($blogCategory);
        $blogCategory->setData([
            'blog_id' => 3, 'category_id' => 3
        ]);
        $this->blogCategory->save($blogCategory);
        $blogCategory->setData([
            'blog_id' => 4, 'category_id' => 4
        ]);
        $this->blogCategory->save($blogCategory);
        $this->getResponse()->setBody('success');
    }
}

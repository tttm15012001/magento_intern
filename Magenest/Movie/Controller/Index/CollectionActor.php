<?php
namespace Magenest\Movie\Controller\Index;

class CollectionActor extends \Magento\Framework\App\Action\Action {
    public function execute() {
        $subscription = $this->_objectManager->create('Magenest\Movie\Model\Actor');
        $subscription->setData([
            //'actor_id' => 13,
            'name' => 'Rowan'
        ]);
        $subscription->save();
        $subscription->setData([
            //'actor_id' => 14,
            'name' => 'Tung'
        ]);
        $subscription->save();
        $subscription->setData([
            //'actor_id' => 15,
            'name' => 'Satoshi'
        ]);
        $subscription->save();
        $subscription->setData([
            //'actor_id' => 15,
            'name' => 'Magenest'
        ]);
        $subscription->save();
        $this->getResponse()->setBody('success');
    }
}

<?php
namespace Magenest\Movie\Controller\Index;

class CollectionDirector extends \Magento\Framework\App\Action\Action {
    public function execute() {
        $subscription = $this->_objectManager->create('Magenest\Movie\Model\Director');
        $subscription->setData(
        [
            //'director_id' => 1,
            'name' => 'Magenest director'
        ]);
        $subscription->save();
        $subscription->setData([
            //'director_id' => 2,
            'name' => 'Magenest'
        ]);
        $subscription->save();
        $subscription->setData([
            //'director_id' => 3,
            'name' => 'Son Tung'
        ]);
        $subscription->save();
        $subscription->setData([
            //'director_id' => 3,
            'name' => 'Cris'
        ]);
        $subscription->save();
        $subscription->setData([
            //'director_id' => 3,
            'name' => 'Magento2'
        ]);
        $subscription->save();
        $this->getResponse()->setBody('success');
    }
}

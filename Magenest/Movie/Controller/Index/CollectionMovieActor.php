<?php
namespace Magenest\Movie\Controller\Index;

use Magento\Framework\App\Action\Context;

class CollectionMovieActor extends \Magento\Framework\App\Action\Action {
    public function execute() {
        $subscription = $this->_objectManager->create('Magenest\Movie\Model\MovieActor');
        $subscription->setData([
            'movie_id' => 1,
            'actor_id' => 1,
        ]);
        $subscription->save();
        $subscription->setData([
            'movie_id' => 2,
            'actor_id' => 2,
        ]);
        $subscription->save();
        $subscription->setData([
            'movie_id' => 2,
            'actor_id' => 3,
        ]);
        $subscription->save();
        $subscription->setData([
            'movie_id' => 3,
            'actor_id' => 3,
        ]);
        $subscription->save();
        $subscription->setData([
            'movie_id' => 4,
            'actor_id' => 4,
        ]);
        $subscription->save();
        $subscription->setData([
            'movie_id' => 4,
            'actor_id' => 1,
        ]);
        $subscription->save();
        $this->getResponse()->setBody('success');
    }
}

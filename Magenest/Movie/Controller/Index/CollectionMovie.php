<?php
namespace Magenest\Movie\Controller\Index;

class CollectionMovie extends \Magento\Framework\App\Action\Action {
    public function execute() {
        $subscription = $this->_objectManager->create('Magenest\Movie\Model\Movie');
        $subscription->setData([
            'name' => 'Harry Potter',
            'description' => 'Fantasy',
            'rating' => 1,
            'director_id' => 1
        ]);
        $subscription->save();
        $subscription->setData([
            'name' => 'Passenger',
            'description' => 'Action',
            'rating' => 2,
            'director_id' => 2
        ]);
        $subscription->save();
        $subscription->setData([
            'name' => 'Insidious',
            'description' => 'Horror',
            'rating' => 4,
            'director_id' => 3
        ]);
        $subscription->save();
        $subscription->setData([
            'name' => 'Pokemon',
            'description' => 'Anime',
            'rating' => 4,
            'director_id' => 4
        ]);
        $subscription->save();
        $this->getResponse()->setBody('success');
    }
}

<?php
namespace Magenest\Movie\Block;

use Magento\Framework\View\Element\Template;

class FrontendAdvanced extends \Magento\Framework\View\Element\Template {
    protected $logger;
    // protected $_template = 'getblogs.phtml';
    public function __construct(
        Template\Context $context,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    )
    {
        $this->logger = $logger;
        parent::__construct($context, $data);
    }

    /*public function testLog() {
        $this->logger->alert('This is my test message for log');
    }*/
}

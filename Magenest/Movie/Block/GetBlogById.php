<?php
namespace Magenest\Movie\Block;

use Magenest\Movie\Model\Blog;
use Magenest\Movie\Model\BlogFactory;
use Magento\Framework\View\Element\Template;

class GetBlogById extends Template {
    private $blog;
    private $collectionBLogFactory;
    private $blogFactory;
    private $request;

    public function  __construct(
        \Magento\Framework\App\Request\Http $request,
        Template\Context $context,
        \Magenest\Movie\Model\BlogFactory $blogFactory,
        \Magenest\Movie\Model\ResourceModel\Blog $blog,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionBLogFactory $collectionBLogFactory,
        array $data = []
    ) {
        $this->request = $request;
        $this->blogFactory = $blogFactory;
        $this->blog = $blog;
        $this->collectionBLogFactory = $collectionBLogFactory;
        return parent::__construct($context, $data);
    }

    public function getContentBlog() {
        $id = $this->request->getParam('id');
        $model = $this->blogFactory->create();
        $resource = $this->blog->load($model, $id);
        //$data = $collection->joinTable();
        return $model->getData('content');
    }
}
?>

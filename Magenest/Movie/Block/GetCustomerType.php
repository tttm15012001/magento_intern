<?php
namespace Magenest\Movie\Block;

use Magento\Framework\View\Element\Template;

class GetCustomerType extends Template {
    private $collectionMovies;
    private $collectionResource;
    private $movieRepository;

    public function  __construct(
        Template\Context $context,
        \Magento\Customer\Model\Customer $customerModel,
        \Magenest\Movie\Model\ResourceModel\Movie $collectionResource,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionMovieFactory $collectionMovies,
        array $data = []
    ) {
        $this->movieRepository = $movieRepository;
        $this->collectionResource = $collectionResource;
        $this->collectionMovies = $collectionMovies;
        return parent::__construct($context, $data);
    }

    public function getMovies() {
        $collection = $this->collectionMovies->create();
        $data = $collection->joinTable();
        return $collection;
    }

    public function getMoviesUsingRepo() {
        return $this->movieRepository->getById(1);
    }
}
?>

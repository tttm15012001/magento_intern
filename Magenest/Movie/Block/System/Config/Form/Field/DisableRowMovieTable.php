<?php
namespace Magenest\Movie\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;

class DisableRowMovieTable extends \Magento\Config\Block\System\Config\Form\Field
{
    private $collectionMovieFactory;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionMovieFactory $collectionMovieFactory
    )
    {
        parent::__construct($context);
        $this->collectionMovieFactory = $collectionMovieFactory;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setData('value', $this->collectionMovieFactory->create()->count());
        $element->setReadonly('true');
        return $element->getElementHtml();
    }
}

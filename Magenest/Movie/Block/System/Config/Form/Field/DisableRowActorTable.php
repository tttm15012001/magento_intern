<?php
namespace Magenest\Movie\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;

class DisableRowActorTable extends \Magento\Config\Block\System\Config\Form\Field
{
    private $collectionActorFactory;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionActorFactory $collectionActorFactory
    )
    {
        parent::__construct($context);
        $this->collectionActorFactory = $collectionActorFactory;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setData('value', $this->collectionActorFactory->create()->count());
        $element->setReadonly('true');
        return $element->getElementHtml();
    }
}

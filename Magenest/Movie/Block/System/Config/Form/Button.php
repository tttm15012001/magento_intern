<?php
namespace Magenest\Movie\Block\System\Config\Form;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Button extends \Magento\Config\Block\System\Config\Form\Field
{
    const BUTTON1_TEMPLATE = 'system/config/button/button.phtml';

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::BUTTON1_TEMPLATE);
        }
        return $this;
    }



    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        //$originalData = $element->getOriginalData();
        $this->addData(
            [
                'id'        => 'reload_button',
                'button_label'     => _('Reload Page'),
                /*'onclick'   => 'javascript:check(); return false;'*/
            ]
        );
        return $this->_toHtml();
    }
}

<?php
namespace Magenest\Movie\Block\Adminhtml\CustomerEdit\Tab;

use Magento\Framework\UrlInterface;

class View extends \Magento\Backend\Block\Template implements \Magento\Ui\Component\Layout\Tabs\TabInterface
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'tab/customer_view.phtml';
    public $tab_label;
    private $storeManager;
    protected $customerModel;
    protected $customerSession;

    /**
     * View constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        UrlInterface $urlBuilder,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Customer $customerModel,
        array $data = [],
        string $tab_label = ''
    ) {
        $this->storeManager = $storeManager;
        $this->_coreRegistry = $registry;
        $this->customerModel = $customerModel;
        $this->tab_label = $data['tab_label'];
        parent::__construct($context, $data);
    }

    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl();
    }

    public function getMediaUrl()
    {
        return $this->getBaseUrl() . 'pub/media/';
    }

    public function getCustomerLogoUrl($logoPath)
    {
        return $this->getMediaUrl() . 'customer' . $logoPath;
    }

    public function getLogoUrl()
    {
        $customerData = $this->customerModel->load($this->getCustomerId());
        $logo = $customerData->getData('avatar');
        if (!empty($logo)) {
            return $this->getCustomerLogoUrl($logo);
        }
        return false;
    }

    /**
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(\Magento\Customer\Controller\RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Custom Tab');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __($this->tab_label);
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return false;
        }
        return true;
    }

    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }
}

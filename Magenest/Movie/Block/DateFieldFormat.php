<?php
namespace Magenest\Movie\Block;

class DateFieldFormat extends \Magento\Config\Block\System\Config\Form\Field
{
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->setDateFormat('dd/mm/yyyy');
        $element->setTimeFormat('HH:mm:ss'); //set date and time as per your need
        $element->setShowsTime(true);
        return parent::render($element);
    }
}

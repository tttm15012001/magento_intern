<?php
namespace Magenest\Movie\Block\Ex4;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as Order;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory as Invoice;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory as Creditmemo;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as Product;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as Customer;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Module\ModuleList\Loader;

class ReportPage extends \Magento\Framework\View\Element\Template {
    private $orderFactory;
    private $invoiceFactory;
    private $creditmemoFactory;
    private $productFactory;
    private $customerFactory;
    private $setup;
    private $moduleLoader;

    public function __construct(
        Template\Context $context,
        Order $orderFactory,
        Invoice $invoiceFactory,
        Creditmemo $creditmemoFactory,
        Product $productFactory,
        Customer $customerFactory,
        SchemaSetupInterface $setup,
        Loader $moduleLoader,
        array $data = []
    )
    {
        $this->moduleLoader = $moduleLoader;
        $this->setup = $setup;
        $this->orderFactory = $orderFactory;
        $this->invoiceFactory = $invoiceFactory;
        $this->creditmemoFactory = $creditmemoFactory;
        $this->productFactory = $productFactory;
        $this->customerFactory = $customerFactory;
        parent::__construct($context, $data);
    }

    public function getModuleInstalledExceptMagento() {
        $table_name = 'setup_module';
        $connection = $this->setup->getConnection();
        $query = $connection->select()->from($table_name)->where(1);
        $fetchData = $connection->fetchAll($query);
        return count($fetchData);
    }

    public function getModuleInstalled() {
        $all = $this->moduleLoader->load();
        return count($all);
    }

    public function getProduct() {
        $collection = $this->productFactory->create();
        return $collection->count();
    }

    public function getCustomer() {
        $collection = $this->customerFactory->create();
        return $collection->count();
    }

    public function getOrder() {
        $collection = $this->orderFactory->create();
        return $collection->count();
    }

    public function getInvoice() {
        $collection = $this->invoiceFactory->create();
        return $collection->count();
    }

    public function getCreditmemo() {
        $collection = $this->creditmemoFactory->create();
        return $collection->count();
    }
}

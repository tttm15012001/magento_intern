<?php
namespace Magenest\Movie\Block;

use Magenest\Movie\Model\Blog;
use Magenest\Movie\Model\BlogFactory;
use Magento\Framework\View\Element\Template;

class GetBlogs extends Template {
    private $blog;
    private $collectionBLogFactory;
    private $blogFactory;

    public function  __construct(
        Template\Context $context,
        \Magenest\Movie\Model\BlogFactory $blogFactory,
        \Magenest\Movie\Model\ResourceModel\Blog $blog,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionBLogFactory $collectionBLogFactory,
        array $data = []
    ) {
        $this->blogFactory = $blogFactory;
        $this->blog = $blog;
        $this->collectionBLogFactory = $collectionBLogFactory;
        return parent::__construct($context, $data);
    }

    public function getBlogs() {
        $model = $this->blogFactory->create();
        $collection = $this->collectionBLogFactory->create();
        foreach ($collection as $item) {
            $resource = $this->blog->load($model, $item->getData('id'));
            $item->setData($model->getData());
        }
        //$data = $collection->joinTable();
        return $collection;
    }
}
?>

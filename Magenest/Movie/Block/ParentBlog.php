<?php
namespace Magenest\Movie\Block;

use Magento\Framework\View\Element\Template;

class ParentBlog extends \Magento\Framework\View\Element\Template {

    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getText() {
        return 'parent text';
    }
}

<?php
namespace Magenest\Movie\Model;
use Magenest\Movie\Api\Data\BlogInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\User\Model\ResourceModel\User;

class Blog extends \Magento\Framework\Model\AbstractModel implements BlogInterface {

    const ENTITY_ID = 'id';
    const AUTHOR_ID = 'author_id';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const CONTENT = 'content';
    const URL_REWRITE = 'url_rewrite';
    const STATUS = 'status';

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_DECLINED = 'declined';

    //public $_eventPrefix = 'magenest_blog';

    private $userFactory;
    private $user;

    public function __construct(
        \Magento\User\Model\UserFactory $userFactory,
        \Magento\User\Model\ResourceModel\User $user,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->user = $user;
        $this->userFactory = $userFactory;
        parent::__construct($context, $registry, $resource,
            $resourceCollection, $data);
    }
    public function _construct() {
        $this->_init('Magenest\Movie\Model\ResourceModel\Blog');
    }

    public function _afterLoad() {
        $users = $this->userFactory->create();
        $user = $this->user->load($users, $this->getData('author_id'));
        $this->setData('author', $users->getData());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setEntityId($entityId)
    {
        $this->setData(self::ENTITY_ID, $entityId);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorId()
    {
        return $this->getData(self::AUTHOR_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthorId($authorId)
    {
        $this->setData(self::AUTHOR_ID, $authorId);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->setData(self::TITLE, $title);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($description)
    {
        $this->setData(self::CONTENT, $description);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUrlRewrite()
    {
        return $this->getData(self::URL_REWRITE);
    }

    /**
     * {@inheritdoc}
     */
    public function setUrlRewrite($urlRewrite)
    {
        $this->setData(self::URL_REWRITE, $urlRewrite);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        $this->setData(self::STATUS, $status);
        return $this;
    }
}

<?php


namespace Magenest\Movie\Model;

use Magento\Framework\Api\SearchResults;
use Magenest\Movie\Api\Data\BlogSearchResultInterface;

/**
 * Class BlogSearchResult
 * @package Magenest\Movie\Model
 */
class BlogSearchResult extends SearchResults implements BlogSearchResultInterface
{

}

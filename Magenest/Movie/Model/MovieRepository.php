<?php

namespace Magenest\Movie\Model;

use Magenest\Movie\Api\Data\MovieInterface;

/**
 * Class MovieRepository
 * @package Magenest\Movie\Model
 */
class MovieRepository implements \Magenest\Movie\Api\MovieRepositoryInterface
{
    /**
     * @var \Magenest\Movie\Model\MovieFactory
     */
    protected $movieFactory;

    /**
     * @var \Magenest\Movie\Model\ResourceModel\Movie
     */
    protected $movieResource;

    /**
     * CustomRepository constructor.
     * @param \Magenest\Movie\Model\MovieFactory $movieFactory
     * @param \Magenest\Movie\Model\ResourceModel\Movie $movieResource
     * @param \Magenest\Movie\Model\ResourceModel\Movie\CollectionMovieFactory $collectionMovieFactory
     */
    public function __construct(
        \Magenest\Movie\Model\MovieFactory $movieFactory,
        \Magenest\Movie\Model\ResourceModel\Movie $movieResource
    ) {
        $this->movieFactory = $movieFactory;
        $this->movieResource = $movieResource;
    }

    /**
     * @param int $id
     * @return \Magenest\Movie\Api\Data\MovieInterface
     */
    public function getById($id)
    {
        $movieModel = $this->movieFactory->create();
        $this->movieResource->load($movieModel, $id);
        if (!$movieModel->getMovieId()) {
            throw new NoSuchEntityException(__('Unable to find custom data with ID "%1"', $id));
        }
        return $movieModel->getData();
    }

    /**
     * @param \Magenest\Movie\Api\Data\MovieInterface $movie
     * @return \Magenest\Movie\Api\Data\MovieInterface
     */
    public function save(MovieInterface $movie)
    {
        $this->movieResource->save($movie);
        return $movie;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id)
    {
        try {
            $customModel = $this->movieFactory->create();
            $this->movieResource->load($customModel, $id);
            $this->movieResource->delete($customModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the entry: %1', $exception->getMessage())
            );
        }

        return true;
    }
}

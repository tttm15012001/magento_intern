<?php
namespace Magenest\Movie\Model\Config;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

class BlogId extends AbstractSource implements SourceInterface, OptionSourceInterface
{
    private $userFactory;
    public function __construct(
        \Magento\User\Model\UserFactory $userFactory
    ) {
        $this->userFactory = $userFactory;
    }

    public function getAllOptions()
    {
        $arrAuthor = $this->userFactory->create()->getCollection();
        $options = [
            ['value' => 0, 'label' => __('Please Choose Director')],
        ];
        foreach ($arrAuthor as $item) {
            array_push($options, ['value' => $item->getUserId(), 'label' => __($item->getFirstname())]);
        }
        return $options;
    }
}

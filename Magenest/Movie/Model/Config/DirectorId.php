<?php
namespace Magenest\Movie\Model\Config;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

class DirectorId extends AbstractSource implements SourceInterface, OptionSourceInterface
{
    private $collectionDirectorFactory;
    public function __construct(
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionDirectorFactory $collectionDirectorFactory
    ) {
        $this->collectionDirectorFactory = $collectionDirectorFactory;
    }

    public function getAllOptions()
    {
        $arrDirector = $this->collectionDirectorFactory->create();
        $options = [
            ['value' => 0, 'label' => __('Please Choose Director')],
        ];
        foreach ($arrDirector as $item) {
            array_push($options, ['value' => $item->getDirectorId(), 'label' => __($item->getName())]);
        }
        return $options;
    }
}

<?php
namespace Magenest\Movie\Model\Config;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

class RegionSource extends AbstractSource implements SourceInterface, OptionSourceInterface
{
    /*private $collectionDirectorFactory;
    public function __construct(
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionDirectorFactory $collectionDirectorFactory
    ) {
        $this->collectionDirectorFactory = $collectionDirectorFactory;
    }*/

    public function getAllOptions()
    {
        //$arrDirector = $this->collectionDirectorFactory->create();
        $options = [
            ['value' => 1, 'label' => __('Bắc')],
            ['value' => 2, 'label' => __('Trung')],
            ['value' => 3, 'label' => __('Nam')]
        ];
        /*foreach ($arrDirector as $item) {
            array_push($options, ['value' => $item->getDirectorId(), 'label' => __($item->getName())]);
        }*/
        return $options;
    }
}

<?php

namespace Magenest\Movie\Model\Config;

use Magenest\Movie\Model\ResourceModel\Movie\CollectionDirectorFactory;

class DirectorProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $_loadedData;
    protected $collection;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionDirectorFactory $collectionFactory,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $item) {
            $this->_loadedData[(int)$item->getData()['director_id']] = $item->getData();
        }
        return $this->_loadedData;
    }
}

<?php
namespace Magenest\Movie\Model\ResourceModel;
use Magento\Framework\DataObject\IdentityInterface;

class BlogCategory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function _construct() {
        $this->_init('magenest_blog_category',
            'blog_id');
    }
}

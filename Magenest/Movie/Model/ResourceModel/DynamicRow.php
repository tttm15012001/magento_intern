<?php
namespace Magenest\Movie\Model\ResourceModel;
use Magento\Framework\DataObject\IdentityInterface;

class DynamicRow extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function _construct() {
        $this->_init('db_ex_9_customize_adminhtml',
            'entity_id');
    }
}

<?php
namespace Magenest\Movie\Model\ResourceModel\Movie;

class CollectionDynamicRowEx9 extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    protected $_idFieldName = 'actor_id';

    public function _construct() {
        $this->_init('Magenest\Movie\Model\DynamicRow',
            'Magenest\Movie\Model\ResourceModel\DynamicRow');
    }
}

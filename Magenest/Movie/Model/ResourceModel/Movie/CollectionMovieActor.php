<?php
namespace Magenest\Movie\Model\ResourceModel\Movie;

class CollectionMovieActor extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    protected $_idFieldName = 'movie_id';

    public function _construct() {
        $this->_init('Magenest\Movie\Model\MovieActor',
            'Magenest\Movie\Model\ResourceModel\MovieActor');
    }
}

<?php
namespace Magenest\Movie\Model\ResourceModel\Movie;

use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CollectionBLog extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    protected $_idFieldName = 'id';

    public function _construct() {
        $this->_init('Magenest\Movie\Model\Blog',
            'Magenest\Movie\Model\ResourceModel\Blog');
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        //$admin_user = $this->getTable('admin_user');
        $this->getSelect()
            ->joinLeft(
                ['au' => $this->getTable('admin_user')],
                'au.user_id = main_table.author_id',
                ['email']
            );
        return $this;
        /*$result = $this
            ->addFieldToSelect('*')
            ->getSelect()
            ->join($admin_user, 'main_table.author_id ='.$admin_user.'.user_id',['authorEmail' => $admin_user.'.email']);
        return $this;*/
    }

    public function joinTable(){
        $admin_user = $this->getTable('admin_user');
        $result = $this
            ->addFieldToSelect('*')
            ->getSelect()
            ->join($admin_user, 'main_table.author_id ='.$admin_user.'.user_id',['authorFirstname' => $admin_user.'.firstname', 'authorLastname' => $admin_user.'.lastname']);
            //->group('main_table.id');
        return $this;
    }
}

<?php
namespace Magenest\Movie\Model\ResourceModel\Movie;

class CollectionBlogCategory extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    protected $_idFieldName = 'blog_id';

    public function _construct() {
        $this->_init('Magenest\Movie\Model\BlogCategory',
            'Magenest\Movie\Model\ResourceModel\BlogCategory');
    }
}

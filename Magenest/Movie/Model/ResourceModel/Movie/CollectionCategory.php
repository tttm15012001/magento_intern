<?php
namespace Magenest\Movie\Model\ResourceModel\Movie;

class CollectionCategory extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    protected $_idFieldName = 'id';

    public function _construct() {
        $this->_init('Magenest\Movie\Model\Category',
            'Magenest\Movie\Model\ResourceModel\Category');
    }
}

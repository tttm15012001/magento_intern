<?php
namespace Magenest\Movie\Model\ResourceModel\Movie;

class CollectionActor extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    protected $_idFieldName = 'actor_id';

    public function _construct() {
        $this->_init('Magenest\Movie\Model\Actor',
            'Magenest\Movie\Model\ResourceModel\Actor');
    }
}

<?php

namespace Magenest\Movie\Model;

use Magenest\Movie\Api\Data\BlogInterface;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionBLog;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;

/**
 * Class BlogRepository
 * @package Magenest\Movie\Model
 */
class BlogRepository implements \Magenest\Movie\Api\BlogRepositoryInterface
{
    /**
     * @var \Magenest\Movie\Model\BlogFactory
     */
    protected $blogFactory;

    /**
     * @var \Magenest\Movie\Model\ResourceModel\Blog
     */
    protected $blogResource;

    /**
     * @var \Magenest\Movie\Model\ResourceModel\Movie\CollectionBlogFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magenest\Movie\Api\Data\BlogSearchResultInterfaceFactory
     */
    protected $searchResultInterfaceFactory;

    /**
     * CustomRepository constructor.
     * @param \Magenest\Movie\Model\BlogFactory $blogFactory
     * @param \Magenest\Movie\Model\ResourceModel\Blog $blogResource
     * @param \Magenest\Movie\Model\ResourceModel\Movie\CollectionBlogFactory $collectionFactory
     * @param \Magenest\Movie\Api\Data\BlogSearchResultInterfaceFactory $searchResultInterfaceFactory
     */
    public function __construct(
        \Magenest\Movie\Model\BlogFactory $blogFactory,
        \Magenest\Movie\Model\ResourceModel\Blog $blogResource,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionBlogFactory $collectionFactory,
        \Magenest\Movie\Api\Data\BlogSearchResultInterfaceFactory $searchResultInterfaceFactory
    ) {
        $this->blogFactory = $blogFactory;
        $this->blogResource = $blogResource;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultInterfaceFactory = $searchResultInterfaceFactory;
    }

    /**
     * @param int $id
     * @return \Magenest\Movie\Api\Data\BlogInterface
     */
    public function getById($id)
    {
        $blogModel = $this->blogFactory->create();
        $this->blogResource->load($blogModel, $id);
        if (!$blogModel->getEntityId()) {
            throw new NoSuchEntityException(__('Unable to find custom data with ID "%1"', $id));
        }
        return $blogModel;
    }

    /**
     * @param \Magenest\Movie\Api\Data\BlogInterface $blog
     * @return \Magenest\Movie\Api\Data\BlogInterface
     */
    public function save(BlogInterface $blog)
    {
        $this->blogResource->save($blog);
        return $blog;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id)
    {
        try {
            $customModel = $this->blogFactory->create();
            $this->blogResource->load($customModel, $id);
            $this->blogResource->delete($customModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the entry: %1', $exception->getMessage())
            );
        }

        return true;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magenest\Movie\Api\Data\BlogSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param CollectionBlog $collection
     */
    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, CollectionBlog $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param CollectionBlog $collection
     */
    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria,
                                               CollectionBlog $collection)
    {
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param CollectionBlog $collection
     */
    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, CollectionBlog $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param CollectionBlog $collection
     * @return mixed
     */
    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, CollectionBlog $collection)
    {
        $searchResults = $this->searchResultInterfaceFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}

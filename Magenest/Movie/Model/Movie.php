<?php
namespace Magenest\Movie\Model;

use Magenest\Movie\Api\Data\MovieInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Movie extends \Magento\Framework\Model\AbstractModel implements MovieInterface {
    protected $_eventPrefix = 'movie_model';

    const MOVIE_ID = 'movie_id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const RATING = 'rating';
    const DIRECTOR_ID = 'director_id';

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_DECLINED = 'declined';
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource,
            $resourceCollection, $data);
    }
    public function _construct() {
        $this->_init('Magenest\Movie\Model\ResourceModel\Movie');
    }

    /**
     * {@inheritdoc}
     */
    public function getMovieId()
    {
        return $this->getData(self::MOVIE_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setMovieId($movieId)
    {
        $this->setData(self::MOVIE_ID, $movieId);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->setData(self::NAME, $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRating()
    {
        return $this->getData(self::RATING);
    }

    /**
     * {@inheritdoc}
     */
    public function setRating($rating)
    {
        $this->setData(self::RATING, $rating);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectorId()
    {
        return $this->getData(self::DIRECTOR_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setDirectorId($directorId)
    {
        $this->setData(self::DIRECTOR_ID, $directorId);
        return $this;
    }
}
